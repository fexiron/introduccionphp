<?php

abstract class Config
{
    const HOST = '/introduccionphp/';
    const ROOT = '/var/www/html/introduccionphp/';

    static function path($file="")
    {
        return Config::HOST . $file;
    }
}